<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Homma homma</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<style>
.ui-slider { margin: 0.7em; }
</style>
<script>


var refreshrate=500;               //milliseconds
var image="kuva.jpeg";		  //image path

var imgheight=480;  
var imgwidth=640;      
     
function refresh(){
document.images["pic"].src=image+"?"+ new Date().getTime() ;


setTimeout('refresh()', refreshrate);
}

function cali(){
            $.ajax({
           type: "POST",
           url: "post_test.php",
           data: {'cali': 1},
           dataType: "json",
           success: function(fromphp) {
           $('#foo').html(fromphp.data);
       }
       });
}

function start(){
cali();
refresh();
}

$(document).ready(function() {
	$( "#slider-range" ).slider({
	range: true,
	min: 0,
	max: 179,
	values: [ 160, 179 ],
	slide: function( event, ui ) {
		$( "#amount" ).val(ui.values[ 0 ] + " - " + ui.values[ 1 ] );
           	$.ajax({
                    	type: 'post',
                    	url: 'post_test.php',
                    	datatype: 'json',
                  	data: {'hue1_min': ui.values[ 0 ], 'hue1_max' : ui.values[ 1 ]},
                    	success: function(fromphp) {
                        $('#foo').html(fromphp.data);
                    }
                });
          
		}	
	});
	$( "#amount" ).val($( "#slider-range" ).slider( "values", 0 ) + " - " + $( "#slider-range" ).slider( "values", 1 ) );
});

$(document).ready(function() {
	$( "#slider-range2" ).slider({
	range: true,
	min: 0,
	max: 255,
	values: [ 0, 255 ],
	slide: function( event, ui ) {
		$( "#amount2" ).val(ui.values[ 0 ] + " - " + ui.values[ 1 ] );
           	$.ajax({
                    	type: 'post',
                    	url: 'post_test.php',
                    	datatype: 'json',
                  	data: {'saturation1_min': ui.values[ 0 ], 'saturation1_max' : ui.values[ 1 ]},
                    	success: function(fromphp) {
                        $('#foo').html(fromphp.data);
                    }
                });
          
		}	
	});
	$( "#amount2" ).val($( "#slider-range2" ).slider( "values", 0 ) + " - " + $( "#slider-range2" ).slider( "values", 1 ) );
});



$(document).ready(function() {
	$( "#slider-range3" ).slider({
	range: true,
	min: 0,
	max: 255,
	values: [ 0, 255 ],
	slide: function( event, ui ) {
		$( "#amount3" ).val(ui.values[ 0 ] + " - " + ui.values[ 1 ] );
           	$.ajax({
                    	type: 'post',
                    	url: 'post_test.php',
                    	datatype: 'json',
                  	data: {'value1_min': ui.values[ 0 ], 'value1_max' : ui.values[ 1 ]},
                    	success: function(fromphp) {
                        $('#foo').html(fromphp.data);
                    }
                });
          
		}	
	});
	$( "#amount3" ).val($( "#slider-range3" ).slider( "values", 0 ) + " - " + $( "#slider-range3" ).slider( "values", 1 ) );
});








$(document).ready(function() {
	$( "#slider-range4" ).slider({
	range: true,
	min: 0,
	max: 179,
	values: [ 0, 20 ],
	slide: function( event, ui ) {
		$( "#amount4" ).val(ui.values[ 0 ] + " - " + ui.values[ 1 ] );
           	$.ajax({
                    	type: 'post',
                    	url: 'post_test.php',
                    	datatype: 'json',
                  	data: {'hue2_min': ui.values[ 0 ], 'hue2_max' : ui.values[ 1 ]},
                    	success: function(fromphp) {
                        $('#foo').html(fromphp.data);
                    }
                });
          
		}	
	});
	$( "#amount4" ).val( $( "#slider-range4" ).slider( "values", 0 ) + " - " + $( "#slider-range4" ).slider( "values", 1 ) );
})





$(document).ready(function() {
	$( "#slider-range5" ).slider({
	range: true,
	min: 0,
	max: 255,
	values: [ 0, 255 ],
	slide: function( event, ui ) {
		$( "#amount5" ).val(ui.values[ 0 ] + " - " + ui.values[ 1 ] );
           	$.ajax({
                    	type: 'post',
                    	url: 'post_test.php',
                    	datatype: 'json',
                  	data: {'saturation2_min': ui.values[ 0 ], 'saturation2_max' : ui.values[ 1 ]},
                    	success: function(fromphp) {
                        $('#foo').html(fromphp.data);
                    }
                });
          
		}	
	});
	$( "#amount5" ).val( $( "#slider-range5" ).slider( "values", 0 ) + " - " + $( "#slider-range5" ).slider( "values", 1 ) );
});

$(document).ready(function() {
	$( "#slider-range6" ).slider({
	range: true,
	min: 0,
	max: 255,
	values: [ 0, 255 ],
	slide: function( event, ui ) {
		$( "#amount6" ).val(ui.values[ 0 ] + " - " + ui.values[ 1 ] );
           	$.ajax({
                    	type: 'post',
                    	url: 'post_test.php',
                    	datatype: 'json',
                  	data: {'value2_min': ui.values[ 0 ], 'value2_max' : ui.values[ 1 ]},
                    	success: function(fromphp) {
                        $('#foo').html(fromphp.data);
                    }
                });
          
		}	
	});
	$( "#amount6" ).val( $( "#slider-range6" ).slider( "values", 0 ) + " - " + $( "#slider-range6" ).slider( "values", 1 ) );
});





//if(document.images)window.onload=refresh;


   $(document).ready(function(){           
     // Removed the unnecessary `$(function() { ...` here and the matching closing bits at the end
     $("#sb").click(function(e){
        e.preventDefault();
        $.ajax({
           type: "POST",
           url: "post_test.php",
           data: {'save': 1},
           dataType: "json",
           success: function(fromphp) {
           $('#foo').html(fromphp.data);
       }
       });
    });
    });
   $(document).ready(function(){           
     // Removed the unnecessary `$(function() { ...` here and the matching closing bits at the end
     $("#sb2").click(function(e){
        e.preventDefault();
        $.ajax({
           type: "POST",
           url: "post_test.php",
           data: {'ccon': 1},
           dataType: "json",
           success: function(fromphp) {
           $('#foo').html(fromphp.data);
       }
       });
    });
    });

	$(document).ready(function(){
		$("#sb3").click(function(e){
		e.preventDefault();
		$.ajax({
		type: "POST",
		url: "post_test.php",
		data: {'dilp': 1},
		dataType: "json",
		success: function(fromphp) {
		$('#foo').html(fromphp.data);
	}
	});
	});
	});
	
	$(document).ready(function(){
		$("#sb4").click(function(e){
		e.preventDefault();
		$.ajax({
		type: "POST",
		url: "post_test.php",
		data: {'dilm': 1},
		dataType: "json",
		success: function(fromphp) {
		$('#foo').html(fromphp.data);
	}
	});
	});
	});

	$(document).ready(function(){
		$("#sb5").click(function(e){
		e.preventDefault();
		$.ajax({
		type: "POST",
		url: "post_test.php",
		data:  {'erop': 1},
		dataType: "json",
		success: function(fromphp) {
		$('#foo').html(fromphp.data);
	}
	});
	});
	});


	$(document).ready(function(){
		$("#sb6").click(function(e){
		e.preventDefault();
		$.ajax({
		type: "POST",
		url: "post_test.php",
		data: {'erom': 1},
		dataType: "json",
		success: function(fromphp) {
		$('#foo').html(fromphp.data);
	}
	});
	});
	});

window.onload=start;      
</script>
</head
<body>

<div id="container" style="width:800px">

	<div id="header" style="background-color:#FFA500;">
<?php include("yla.php"); ?>

	</div>




	<div id="kuva" style="background-color:#FFD700;height:484px;width:644px;float:left;">
<img src="'+image+'" height="'+imgheight+'" width="'+imgwidth+'"name="pic" style="float: left; border-width: 2px; border-style: solid;">
	</div>

	<div id="content" style="background-color:#EEEEEE;height:484px;width:156px;float:left;">




		<p>
		<label for="amount">Hue1:</label>
		<input type="text" id="amount" style="border:0; color:#f6931f; font-weight:bold;width:70px;">
		</p>
		<div id="slider-range"></div>

		<p>
		<label for="amount2">Saturation1:</label>
		<input type="text" id="amount2" style="border:0; color:#f6931f; font-weight:bold;width:70px;">
		</p>
		<div id="slider-range2"></div>

		<p>
		<label for="amount3">Value1:</label>
		<input type="text" id="amount3" style="border:0; color:#f6931f; font-weight:bold;width:70px;">
		</p>
		<div id="slider-range3"></div>


		<p>
		<label for="amount4">Hue2</label>
		<input type="text" id="amount4" style="border:0; color:#f6931f; font-weight:bold;width:70px;">
		</p>
		<div id="slider-range4"></div>

		<p>
		<label for="amount5">Saturation2:</label>
		<input type="text" id="amount5" style="border:0; color:#f6931f; font-weight:bold;width:70px;">
		</p>
		<div id="slider-range5"></div>

		<p>
		<label for="amount6">Value2:</label>
		<input type="text" id="amount6" style="border:0; color:#f6931f; font-weight:bold;width:70px;">
		</p>
		<div id="slider-range6"></div>

		
	</div>
	<div id="footer" style="background-color:#FFA500;">
		<p><input type ="button" id = "sb"value="Save"/> <input type ="button" id = "sb2"value="Continue"/>
		<input type ="button" id = "sb3"value="dil+"/> <input type ="button" id = "sb4"value="dil-"/>
		<input type ="button" id = "sb5"value="ero+"/> <input type ="button" id = "sb6"value="ero-"/>
		</p>
	</div>
<div id="foo">Data returned from php will show here.</div>
</div>
 
</body>
</html>
