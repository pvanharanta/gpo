#include <pthread.h>
#include <stdio.h>
#include <stdlib.h> //exit
#include <string.h> //memset
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include "cv.h"
#include "highgui.h"

#define HUE1_MIN 160
#define HUE1_MAX 179

#define SATURATION1_MIN 0
#define SATURATION1_MAX 255

#define VALUE1_MIN 0
#define VALUE1_MAX 255


#define HUE2_MIN 0
#define HUE2_MAX 20

#define SATURATION2_MIN 0
#define SATURATION2_MAX 255

#define VALUE2_MIN 0
#define VALUE2_MAX 255

#define DILATE_ITE 0
#define ERODE_ITE 0


#define COMMAND_NONE		0
#define COMMAND_START_CALI	10
#define COMMAND_CALI_SAVE	11
#define COMMAND_START_CALCULATE 20
#define COMMAND_SAVE_CALCULATE	21

typedef struct cali{
	pthread_mutex_t mutex;
	int hue1_min;
	int hue1_max;
	int sat1_min;
	int sat1_max;
	int val1_min;
	int val1_max;

	int hue2_min;
	int hue2_max;
	int sat2_min;
	int sat2_max;
	int val2_min;
	int val2_max;
	int dilate_ite;
	int erode_ite;
}cali;

/*Global variables start*/

cali vcali = {
.mutex = PTHREAD_MUTEX_INITIALIZER,

.hue1_min = HUE1_MIN,
.hue1_max = HUE1_MAX,
.sat1_min = SATURATION1_MIN,
.sat1_max = SATURATION1_MAX,
.val1_min = VALUE1_MIN,
.val1_max = VALUE1_MAX,

.hue2_min = HUE2_MIN,
.hue2_max = HUE2_MAX,
.sat2_min = SATURATION2_MIN,
.sat2_max = SATURATION2_MAX,
.val2_min = VALUE2_MIN,
.val2_max = VALUE2_MAX,
.dilate_ite = DILATE_ITE,
.erode_ite = ERODE_ITE,
};

int command = COMMAND_NONE;
CvRect rect;

/*Global variables end*/


/********************************************************
 * Function error					*
 * Print error message. Unlock mutex and stop program	*
 * 							*
 * parameters:						*
 * - msg, Error message					*
 ********************************************************
 * 8.7.2014 Init version				*
 ********************************************************/

void error(char *msg)
{
	perror(msg);
	pthread_mutex_unlock(&vcali.mutex);
	exit(1);
}


/********************************************************
 * Function calibration					*
 * Makes hsv transform from frame. Make two different 	*
 * threshold frames. Add those thresholded frame's	*
 * thogeter. Make dilate and erode for thresholded	*
 * frame if needed. Save thresholded frame to web server*
 * folder. If command from UI is save. Calculate biggest*
 * countour area and save area rectagle information to	*
 * global variable rect.				*
 * 							*
 * parameters:						*
 * - frame, Live image from camera			*
 * - saved, Saved calibration image			*
 * - hsv_frame, Image for hsv transform			*
 * - thresholded, Threshold image			*
 * - thresholded2, Threshold image			*
 * - element, Element for dilate and erode		*
 * - contours, Contours data				*
 * - storage, Memory storage for contour		*
 ********************************************************
 * 8.7.2014 Init version				*
 ********************************************************/

void calibration(IplImage *frame, IplImage *saved, IplImage *hsv_frame, IplImage *thresholded, IplImage *thresholded2,
IplConvKernel *element, CvSeq* contours, CvMemStorage* storage)
{
	int i=0;
	int cnt = 0;
	int largest_area=0; 
	double a;	

	cvCvtColor(frame, hsv_frame, CV_BGR2HSV);

	cvSaveImage("/var/www/hsv.jpeg",hsv_frame,NULL);
	pthread_mutex_lock(&vcali.mutex);
	cvInRangeS(hsv_frame, cvScalar(vcali.hue1_min,vcali.sat1_min,vcali.val1_min,0),
				 cvScalar(vcali.hue1_max,vcali.sat1_max,vcali.val1_max,0), thresholded);

	cvInRangeS(hsv_frame, cvScalar(vcali.hue2_min,vcali.sat2_min,vcali.val2_min,0),
				 cvScalar(vcali.hue2_max,vcali.sat2_max,vcali.val2_max,0), thresholded2);

	pthread_mutex_unlock(&vcali.mutex);

	cvSaveImage("/var/www/th1.jpeg",thresholded,NULL);
	cvSaveImage("/var/www/th2.jpeg",thresholded2,NULL);
 	cvAdd(thresholded, thresholded2, thresholded, NULL);
		
	cvDilate(thresholded,thresholded,element,vcali.dilate_ite);
	cvErode(thresholded,thresholded,element,vcali.erode_ite);

	pthread_mutex_unlock(&vcali.mutex);
	cvWaitKey(500);  // Wait for 500 ms for user to hit any key

	cvSaveImage("/var/www/kuva.jpeg", thresholded, NULL);

	if (command == COMMAND_CALI_SAVE){
		printf("Save pressed\n");
		cvCopy(frame,saved,0);
		cnt = cvFindContours( thresholded, storage, &contours, sizeof(CvContour),
		CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, cvPoint(0,0) );
		for(; contours!=0; contours = contours->h_next)
		{
                //  Find the area of contour
      			a=cvContourArea( contours, CV_WHOLE_SEQ,0);
		
			if(a>largest_area){
 
        		// Store the index of largest contour            
        		// Find the bounding rectangle for biggest contour
        		rect=cvBoundingRect(contours, 0);
			largest_area = a;
        		}
		}

		cvRectangle(saved,                  
		cvPoint(rect.x, rect.y),    
		cvPoint(rect.x+rect.width, rect.y+rect.height),
		cvScalar(255, 0,0, 0),
			2, 8, 0); 
	
		while(command==COMMAND_CALI_SAVE)
			{
			}
		printf("Continue pressed\n");		
	}
}


/********************************************************
 * Function calculate					*
 * Clone frame and saved frame. If command is save. Set *
 * cloned frame's ROI from global variable rect. 	*
 * Calclulate diffent's from frame and saved. Make	*
 * calculated frame to gray format and thresholded it.	*
 * Make erode and dilate for removing noise. Count non	*
 * zero pixel and calculate cover procent. Write procent*
 * value to orginal frame. Release function only used	*
 * image pointer's					*
 * 							*
 * parameters:						*
 * - frame, Live image from camera			*
 * - saved, Saved calibration image			*
 * - element, Element for dilate and erode		*
 * - font, Font for image text				*
 ********************************************************
 * 8.7.2014 Init version				*
 ********************************************************/

void calculate(IplImage *frame, IplImage *saved, IplConvKernel *element, CvFont font)
{
	char buffer[25] = "Hello World!";
	int pixel_count1,pixel_count2;
	double pixel_procent;
	IplImage* frame_live = cvCloneImage(frame);
	IplImage* frame_saved = cvCloneImage(saved);
	IplImage *subImage = cvCloneImage(frame_live);
	IplImage *subImage_gray = cvCreateImage(cvSize(rect.width, rect.height),IPL_DEPTH_8U,1);

	if(command == COMMAND_SAVE_CALCULATE)
	{
		pixel_count1 = 0;
		pixel_count2 = 0;

		cvSetImageROI(frame_live,  rect);
		cvSetImageROI(frame_saved,  rect); 
		subImage = cvCloneImage(frame_live); 

		cvAbsDiff(frame_live,frame_saved,subImage);
		
		cvCvtColor(subImage, subImage_gray, CV_BGR2GRAY);
		cvThreshold( subImage_gray, subImage_gray, 50, 255, CV_THRESH_BINARY);
		
		cvErode(subImage_gray, subImage_gray,element,1);
		cvDilate(subImage_gray, subImage_gray,element,1);

		cvDilate(subImage_gray, subImage_gray,element,1);
		cvErode(subImage_gray, subImage_gray,element,1);		
		
		pixel_count1 = cvCountNonZero(subImage_gray);
		pixel_count2 = rect.width*rect.height;

		pixel_procent = ((double)pixel_count1/(double)pixel_count2)*100;
		cvSaveImage("/var/www/live.jpeg", frame_live, NULL);
		cvSaveImage("/var/www/saved.jpeg", frame_saved, NULL);
		cvSaveImage("/var/www/sub.jpeg", subImage, NULL);
		cvSaveImage("/var/www/sub1.jpeg", subImage_gray, NULL);
		
		sprintf(buffer,"%.2f", pixel_procent);
		
		cvWaitKey(500);  // Wait for 500 ms for user to hit any key
		cvRectangle(frame,                  
		cvPoint(rect.x, rect.y),    
		cvPoint(rect.x+rect.width, rect.y+rect.height),
		cvScalar(255, 0,0, 0),
			2, 8, 0);
	
		cvPutText(frame, buffer, cvPoint(30, 30), &font, cvScalar(255,0,0,0));
		cvSaveImage("/var/www/calc.jpeg", frame, NULL);
	
		while(command==COMMAND_SAVE_CALCULATE)
			{
			}
		printf("Continue pressed\n");	
	}else{
	
	cvWaitKey(500);  // Wait for 500 ms for user to hit any key
	cvRectangle(frame,                  
		cvPoint(rect.x, rect.y),    
		cvPoint(rect.x+rect.width, rect.y+rect.height),
		cvScalar(255, 0,0, 0),
			2, 8, 0); 	
	cvSaveImage("/var/www/calc.jpeg", frame, NULL);
	}
	cvReleaseImage(&frame_live);
	cvReleaseImage(&frame_saved);
	cvReleaseImage(&subImage);
	cvReleaseImage(&subImage_gray);

}


/********************************************************
 * Function message_handler				*
 * Split message to piece's. Piece's is eroded with 	*
 * char ",". Check which command is received and make 	*
 * global variable modifies.				*
 *							*
 * Command list						*
 * - hue1, Hue1 values changed				*
 * - sat1, Saturation1 values changed			*
 * - val1, Value1 values changed			*
 * - hue2, Hue2 values changed				*
 * - sat2, Saturation2 values changed			*
 * - val2, Value2 values changed			*
 * - save, Calibration save				*
 * - ccon, Calibration continue				*
 * - cali, Calibration start				*
 * - calc, Calculation continue				*
 * - cals, Calculation save				*
 * - dilp, Add one to dilate iteration			*
 * - dilm, Dec one to dilate iteration			*
 * - erop, Add one to erode iteration			*
 * - erom, Dec one to erode iteration			*
 *							*
 * parameters:						*
 * - buffer, received message data			*
 ********************************************************
 * 8.7.2014 Init version				*
 ********************************************************/

void message_handler(char buffer[256])
{
	int k;	
	char strArray[3][5];
	char * pch;

		k=0;
		pch = strtok (buffer,",");
		while(pch != NULL)
		{

			strcpy(strArray[k], pch);

			pch = strtok(NULL, ",");
			k++;
		}

		if(strcmp( strArray[0], "hue1")==0)
		{
			pthread_mutex_lock(&vcali.mutex);
			vcali.hue1_min = atoi(strArray[1]);
			vcali.hue1_max = atoi(strArray[2]);
			pthread_mutex_unlock(&vcali.mutex);


		} else 	if(strcmp( strArray[0], "sat1")==0)
		{
			pthread_mutex_lock(&vcali.mutex);
			vcali.sat1_min = atoi(strArray[1]);
			vcali.sat1_max = atoi(strArray[2]);
			pthread_mutex_unlock(&vcali.mutex);


		} else 	if(strcmp( strArray[0], "val1")==0)
		{
			pthread_mutex_lock(&vcali.mutex);
			vcali.val1_min = atoi(strArray[1]);
			vcali.val1_max = atoi(strArray[2]);
			pthread_mutex_unlock(&vcali.mutex);


		} else 	if(strcmp( strArray[0], "hue2")==0)
		{
			pthread_mutex_lock(&vcali.mutex);
			vcali.hue2_min = atoi(strArray[1]);
			vcali.hue2_max = atoi(strArray[2]);
			pthread_mutex_unlock(&vcali.mutex);


		} else 	if(strcmp( strArray[0], "sat2")==0)
		{
			pthread_mutex_lock(&vcali.mutex);
			vcali.sat2_min = atoi(strArray[1]);
			vcali.sat2_max = atoi(strArray[2]);
			pthread_mutex_unlock(&vcali.mutex);


		} else 	if(strcmp( strArray[0], "val2")==0)
		{
			pthread_mutex_lock(&vcali.mutex);
			vcali.val2_min = atoi(strArray[1]);
			vcali.val2_max = atoi(strArray[2]);
			pthread_mutex_unlock(&vcali.mutex);


		} else if(strcmp( strArray[0], "save")==0)
		{
			command = COMMAND_CALI_SAVE;
		} else if(strcmp( strArray[0], "ccon")==0)
		{
			command = COMMAND_START_CALI;
		} else if(strcmp( strArray[0], "cali")==0)
		{
			command = COMMAND_START_CALI;
		}else if(strcmp( strArray[0], "calc")==0)
		{
			command = COMMAND_START_CALCULATE;
		}else if(strcmp( strArray[0], "cals")==0)
		{
			command = COMMAND_SAVE_CALCULATE;
		}else if(strcmp( strArray[0], "ccal")==0)
		{
			command = COMMAND_START_CALCULATE;
		}else if(strcmp( strArray[0], "dilp")==0)
		{
			pthread_mutex_lock(&vcali.mutex);
			vcali.dilate_ite++;
			printf("%d\n",vcali.dilate_ite);
			pthread_mutex_unlock(&vcali.mutex);
		}else if(strcmp(strArray[0], "dilm")==0)
		{
			pthread_mutex_lock(&vcali.mutex);
			vcali.dilate_ite--;
			if(vcali.dilate_ite < 0)vcali.dilate_ite=0;
			printf("%d\n",vcali.dilate_ite);
			pthread_mutex_unlock(&vcali.mutex);
		}else if(strcmp(strArray[0], "erop")==0)
		{
			pthread_mutex_lock(&vcali.mutex);
			vcali.erode_ite++;
			printf("%d\n",vcali.erode_ite);
			pthread_mutex_unlock(&vcali.mutex);
		}else if(strcmp(strArray[0], "erom")==0)
		{
			pthread_mutex_lock(&vcali.mutex);
			vcali.erode_ite--;
			if(vcali.erode_ite < 0)vcali.erode_ite=0;
			printf("%d\n",vcali.erode_ite);
			pthread_mutex_unlock(&vcali.mutex);
		} else 
		{
			printf("Unknow command\n");
		}
}

/********************************************************
 * Function camera_thread				*
 * Init used image's, element, contour, memstorage and	*
 * font. Start capturing frame's from camera module.	*
 * In loop check which is command variable value and do *
 * wanted function.					*
 * 							*
 * parameters:						*
 * - arg, NULL						* 
 ********************************************************
 * 8.7.2014 Init version				*
 ********************************************************/

void *camera_thread(void *arg) {

	IplImage *frame; // Pointer to current frame
	IplImage *saved;
	IplImage *hsv_frame ;
	IplImage *thresholded ;
	IplImage *thresholded2 ;

	IplConvKernel *element = cvCreateStructuringElementEx(5,5,1,1,CV_SHAPE_RECT,0);

	CvSeq* contours = 0;
	CvMemStorage* storage = cvCreateMemStorage(0);

	CvFont font;
	cvInitFont(&font, CV_FONT_HERSHEY_SIMPLEX, 1.0, 1.0, 0, 1, 8);

	CvCapture *capture = cvCaptureFromCAM(CV_CAP_ANY);
  
	frame = cvQueryFrame(capture);
 
	saved = cvCreateImage(cvSize(frame->width,frame->height), 8, 3);

	hsv_frame = cvCreateImage(cvGetSize(frame), IPL_DEPTH_8U, 3);
	thresholded = cvCreateImage(cvGetSize(frame), IPL_DEPTH_8U, 1);
	thresholded2 = cvCreateImage(cvGetSize(frame), IPL_DEPTH_8U, 1);
 
	while(1)
	{
		frame = cvQueryFrame(capture);
		if(command == COMMAND_START_CALI || command == COMMAND_CALI_SAVE){
			calibration(frame,saved,hsv_frame,thresholded,thresholded2,element,contours,storage);
		}
		if(command == COMMAND_START_CALCULATE || command == COMMAND_SAVE_CALCULATE){
			calculate(frame,saved,element,font);
		}
	}
 
   	/* Release All Images and Windows */
	cvReleaseCapture(&capture);

	cvReleaseImage(&frame);
	cvReleaseImage(&saved);
	cvReleaseImage(&hsv_frame);
	cvReleaseImage(&thresholded);
	cvReleaseImage(&thresholded2);

	cvReleaseStructuringElement(&element);

	cvReleaseMemStorage(&storage);
	pthread_exit(NULL);
}

/********************************************************
 * Function main					*
 * Init camera_thread. Open new socket. Socket port is	*
 * received from program start line. Read data from	*
 * socket when somebody write there.			*
 * 							*
 * parameters:						*
 * - argc, 						*
 * - argv, 
 ********************************************************
 * 8.7.2014 Init version				*
 ********************************************************/

int main(int argc, char *argv[]) {
	pthread_t thread;
	int i,j;
	int sockfd, newsockfd, portno, clilen;
	char buffer[256];


	struct sockaddr_in serv_addr, cli_addr;
	int n;

	if (argc < 2) {
		fprintf(stderr,"ERROR, no port provided\n");
		exit(1);
	}
     	
	if (pthread_create(&thread, NULL, &camera_thread, NULL)) {
		exit(0);
	}

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) 
		error("ERROR opening socket");

	memset((char *) &serv_addr ,0,sizeof(serv_addr));
	portno = atoi(argv[1]);

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portno);
	if (bind(sockfd, (struct sockaddr *) &serv_addr,
		sizeof(serv_addr)) < 0) 
		error("ERROR on binding");

	listen(sockfd,20);
	clilen = sizeof(cli_addr);

	while (1)
	{
		newsockfd = accept(sockfd, 
			(struct sockaddr *) &cli_addr, 
			&clilen);

		if (newsockfd < 0) 
			error("ERROR on accept");

		memset(buffer,0,256);
		n = read(newsockfd,buffer,255);

		if (n < 0) error("ERROR reading from socket");

		printf("\nThe message: %s\n",buffer);

		message_handler(buffer);
	}

	pthread_exit(NULL);
}
