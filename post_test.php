<?php
 
/*
 * PHP Sockets - How to create a sockets server/client
 */
 
//include the server.php script to start the server
//include_once('server.php');

$host    = "127.0.0.1";
$port    = 4096;

if (isset($_POST['hue1_min']) && isset($_POST['hue1_max'])) {

	$command = "hue1";
	$value_min = $_POST['hue1_min'];
	$value_max = $_POST['hue1_max'];   

	$message = $command.",".$value_min.",".$value_max;

	// create socket
	$socket = socket_create(AF_INET, SOCK_STREAM, 0) or die("Could not create socket\n");
	// connect to server
	$result = socket_connect($socket, $host, $port) or die("Could not connect to server\n"); 
	// send string to server
	socket_write($socket, $message, strlen($message)) or die("Could not send data to server\n");
	// close socket
	socket_close($socket);

	@header("Content-type: application/json");
	echo json_encode(array('data' => $_POST['hue1_min']));

	die();

} elseif(isset($_POST['saturation1_min']) && isset($_POST['saturation1_max'])) {

	$command = "sat1";
	$value_min = $_POST['saturation1_min'];
	$value_max = $_POST['saturation1_max'];   

	$message = $command.",".$value_min.",".$value_max;

	// create socket
	$socket = socket_create(AF_INET, SOCK_STREAM, 0) or die("Could not create socket\n");
	// connect to server
	$result = socket_connect($socket, $host, $port) or die("Could not connect to server\n"); 
	// send string to server
	socket_write($socket, $message, strlen($message)) or die("Could not send data to server\n");
	// close socket
	socket_close($socket);

	@header("Content-type: application/json");
	echo json_encode(array('data' => $_POST['saturation1_min']));

	die();

} elseif(isset($_POST['value1_min']) && isset($_POST['value1_max'])) {

	$command = "val1";
	$value_min = $_POST['value1_min'];
	$value_max = $_POST['value1_max'];   

	$message = $command.",".$value_min.",".$value_max;

	// create socket
	$socket = socket_create(AF_INET, SOCK_STREAM, 0) or die("Could not create socket\n");
	// connect to server
	$result = socket_connect($socket, $host, $port) or die("Could not connect to server\n"); 
	// send string to server
	socket_write($socket, $message, strlen($message)) or die("Could not send data to server\n");
	// close socket
	socket_close($socket);

	@header("Content-type: application/json");
	echo json_encode(array('data' => $_POST['value1_min']));

	die();

} elseif (isset($_POST['hue2_min']) && isset($_POST['hue2_max'])) {

	$command = "hue2";
	$value_min = $_POST['hue2_min'];
	$value_max = $_POST['hue2_max'];   

	$message = $command.",".$value_min.",".$value_max;

	// create socket
	$socket = socket_create(AF_INET, SOCK_STREAM, 0) or die("Could not create socket\n");
	// connect to server
	$result = socket_connect($socket, $host, $port) or die("Could not connect to server\n"); 
	// send string to server
	socket_write($socket, $message, strlen($message)) or die("Could not send data to server\n");
	// close socket
	socket_close($socket);

	@header("Content-type: application/json");
	echo json_encode(array('data' => $_POST['hue2_min']));

	die();

} elseif(isset($_POST['saturation2_min']) && isset($_POST['saturation2_max'])) {

	$command = "sat2";
	$value_min = $_POST['saturation2_min'];
	$value_max = $_POST['saturation2_max'];   

	$message = $command.",".$value_min.",".$value_max;

	// create socket
	$socket = socket_create(AF_INET, SOCK_STREAM, 0) or die("Could not create socket\n");
	// connect to server
	$result = socket_connect($socket, $host, $port) or die("Could not connect to server\n"); 
	// send string to server
	socket_write($socket, $message, strlen($message)) or die("Could not send data to server\n");
	// close socket
	socket_close($socket);

	@header("Content-type: application/json");
	echo json_encode(array('data' => $_POST['saturation2_min']));

	die();

} elseif(isset($_POST['value2_min']) && isset($_POST['value2_max'])) {

	$command = "val2";
	$value_min = $_POST['value2_min'];
	$value_max = $_POST['value2_max'];   

	$message = $command.",".$value_min.",".$value_max;

	// create socket
	$socket = socket_create(AF_INET, SOCK_STREAM, 0) or die("Could not create socket\n");
	// connect to server
	$result = socket_connect($socket, $host, $port) or die("Could not connect to server\n"); 
	// send string to server
	socket_write($socket, $message, strlen($message)) or die("Could not send data to server\n");
	// close socket
	socket_close($socket);

	@header("Content-type: application/json");
	echo json_encode(array('data' => $_POST['value2_min']));

	die();

}elseif(isset($_POST['save'])) {

	$command = "save";

	$message = $command;

	// create socket
	$socket = socket_create(AF_INET, SOCK_STREAM, 0) or die("Could not create socket\n");
	// connect to server
	$result = socket_connect($socket, $host, $port) or die("Could not connect to server\n"); 
	// send string to server
	socket_write($socket, $message, strlen($message)) or die("Could not send data to server\n");
	// close socket
	socket_close($socket);

	@header("Content-type: application/json");
	echo json_encode(array('data' => $_POST['save']));

	die();

}elseif(isset($_POST['ccon'])) {

	$command = "ccon";

	$message = $command;

	// create socket
	$socket = socket_create(AF_INET, SOCK_STREAM, 0) or die("Could not create socket\n");
	// connect to server
	$result = socket_connect($socket, $host, $port) or die("Could not connect to server\n"); 
	// send string to server
	socket_write($socket, $message, strlen($message)) or die("Could not send data to server\n");
	// close socket
	socket_close($socket);

	@header("Content-type: application/json");
	echo json_encode(array('data' => $_POST['ccon']));

	die();

}elseif(isset($_POST['cali'])) {

	$command = "cali";

	$message = $command;

	// create socket
	$socket = socket_create(AF_INET, SOCK_STREAM, 0) or die("Could not create socket\n");
	// connect to server
	$result = socket_connect($socket, $host, $port) or die("Could not connect to server\n"); 
	// send string to server
	socket_write($socket, $message, strlen($message)) or die("Could not send data to server\n");
	// close socket
	socket_close($socket);

	@header("Content-type: application/json");
	echo json_encode(array('data' => $_POST['cali']));

	die();

}elseif(isset($_POST['dilp'])) {
	
	$command = "dilp";
	$message = $command;
	$socket = socket_create(AF_INET,SOCK_STREAM,0) or die("Could not create socket\n");
	$result = socket_connect($socket, $host, $port) or die("Could not connect to server\n");
	socket_write($socket, $message, strlen($message)) or die("Could not send data to server\n");
	socket_close($socket);

	@header("Content-type: application/json");
	echo json_encode(array('data' => $_POST['dilp']));
	die();
}elseif(isset($_POST['dilm'])) {
	$command = "dilm";
	$message = $command;
	$socket = socket_create(AF_INET, SOCK_STREAM, 0) or die("Could not create socket\n");
	$result = socket_connect($socket, $host, $port) or die("Could not connect to server\n");
	socket_write($socket, $message, strlen($message)) or die("Could not send data to server\n");
	socket_close($socket);

	@header("Content-type: application/json");
	echo json_encode(array('data' => $_POST['dilm']));
	die();
}elseif(isset($_POST['erop'])) {
	$command = "erop";
	$message = $command;
	$socket = socket_create(AF_INET, SOCK_STREAM, 0) or die("Could not create socket\n");
	$result = socket_connect($socket, $host, $port) or die("Could not connect to server\n");
	socket_write($socket, $message, strlen($message)) or die("Could not send data to server");
	socket_close($socket);

	@header("Content-type: application/json");
	echo json_encode(array('data' => $_POST['erop']));
	die();
}elseif(isset($_POST['erom'])) {
	$command = "erom";
	$message = $command;
	$socket = socket_create(AF_INET, SOCK_STREAM,0) or die("Could not create socket\n");
	$result = socket_connect($socket, $host, $port) or die("Could not connect to server\n");
	socket_write($socket, $message, strlen($message)) or die("Could not send data to server");
	socket_close($socket);

	@header("Content-type: application/json");
	echo json_encode(array('data' => $_POST['erom']));
	die();
}

else {}

?>





